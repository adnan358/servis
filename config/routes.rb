Rails.application.routes.draw do
  resources :subcontractors do
    collection do
      get 'list'
    end
  end
  root :to => 'application#application'

  devise_for :users, controllers: {
        sessions: 'users/sessions'
      }

  resources :services do
    collection do
      get 'list'
    end
  end
  resources :brands
  resources :districts
  resources :payment_types
  resources :device_types
  resources :create_device_types
  resources :data do
    collection do
      get 'service_list'
      get 'customer_list'
    end
  end

  resources :customers do
    collection do
      get 'list'
    end
  end

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
