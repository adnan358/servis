json.extract! subcontractor, :id, :name, :transaction, :user, :description, :service, :created_at, :updated_at
json.url subcontractor_url(subcontractor, format: :json)
