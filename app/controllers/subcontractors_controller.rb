class SubcontractorsController < ApplicationController
  before_action :set_subcontractors, only: [:index, :list, :show, :edit, :update, :destroy]

  # GET /subcontractors
  # GET /subcontractors.json
  def index
    @subcontractors = Subcontractor.all
  end

  # GET /subcontractors/1
  # GET /subcontractors/1.json
  def show
    @deletable = "Taşeronu Sil"
    render_modal "Taşeron Düzenleme"
  end

  # GET /subcontractors/new
  def new
    @subcontractor = Subcontractor.new
    render_modal "Taşeron Ekleme"
  end

  # GET /subcontractors/1/edit
  def edit
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_subcontractors
      @subcontractors = current_user.subcontractors.order(created_at: :asc)
      @subcontractor = Subcontractor.find(params[:id]) rescue nil
    end

    # Only allow a list of trusted parameters through.
    def subcontractor_params
      params.fetch(:subcontractor, {}).permit(:name, :credit, :description)
    end
end
