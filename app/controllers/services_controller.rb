class ServicesController < ApplicationController
  before_action :set_services, only: [:index, :list]
  protect_from_forgery except:[ :show, :new ]

  def show
    @deletable = "Servisi Sil"
    render_modal "Servis Detayı"
  end

  def new
    render_modal "Yeni Servis"
  end

  def create
    if @object.save
      flash[:success] = "Servis oluşturuldu"
      respond_to do |format|
        format.js { render "#{params[:action]}"}
        format.html { render "#{params[:action]}"}
      end
    else
      render partial: "errors"
    end
  end

  def update
    if @object.update(permit_params)
      flash[:success] = "Servis Güncellendi"
      respond_to do |format|
        format.js { render "#{params[:action]}" }
        format.html { render "#{params[:action]}"}
      end
    else
      render partial: "errors"
    end
  end

  def list
    render "list"
  end

  private

  def set_services
    @services = current_user.services.order(created_at: :asc)
  end
    # Only allow a list of trusted parameters through.
    def permit_params
      params.fetch(:service, {}).permit(:name, :phone, :address, :district_id, :device_type_id, :brand_id, :defect, :description, :appointment_date, :subcontractor, :customer_id, :subcontractor_id, :status)
    end
end
