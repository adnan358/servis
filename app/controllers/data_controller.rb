class DataController < ApplicationController

  # Menu search
  def service_list
    q = Customer.ransack("phone_or_name_start": params[:q])
    @customer = q.result(distinct: true).map do |customer|
      {
        title: customer.name,
        description: customer.full_address,
        link: edit_customer_path(customer)
      }
    end
    render json: { results: @customer }
  end

  # Service modal
  def customer_list
    q = Customer.ransack("phone_or_name_start": params[:q])
    if params[:q].blank?
      customer = q.result(distinct: true).limit(10)
    else
      customer = q.result(distinct: true)
    end

    @customers = customer.map do |customer|
      {
        name: customer.name,
        value: customer.id
      }
    end
    render json: { results: @customers }
  end
end
