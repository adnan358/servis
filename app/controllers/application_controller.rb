class ApplicationController < ActionController::Base
  before_action :authenticate_user!
  layout 'application'

  before_action :set_current_user
  before_action :set_object, only: [:show, :edit, :update, :destroy, :new, :create]

  def application
    if user_signed_in?
      redirect_to services_path
    end
  end

  def show
    default_response
  end

  # GET /services/1/edit
  def edit
  end

  def new
    default_response
  end

  def create
    if @object.save!
      flash[:success] = "Başarıyla Oluşturuldu"
    end
    default_response
  end

  def update
    if @object.update(get_permit_params)
      flash[:update] = 'Taşeron güncellendi'
      default_response
    end
  end

  def destroy
    if @object.destroy
      flash[:destroy] = "Başarıyla silindi"
    else
      flash[:destroy] = @object.errors.full_messages.first || "Silerken problem oluştu!"
      flash[:toast_class] = "error"
    end
    default_response
  end

  private

  def get_permit_params
    if self.respond_to?(model.to_s.parameterize + "_params", true)
      send(model.to_s.parameterize + "_params")
    else
      permit_params
    end
  end

  def default_response
    respond_to do |format|
      format.js { render template: "shared/#{params[:action]}"}
      format.html { render template: "shared/#{params[:action]}"}
    end
  end

  def render_modal(title = nil, options = {})
    @modal_title = title || (translation_model_name + " " + params[:action]).capitalize
    render "shared/modals/_show_modal", locals: options
  end

  def model_name
    # Example "Services"
    @model_name = controller_path.classify
    @model_name
  end

  def model
    model_name.constantize rescue nil
  end

  def translation_model_name
    t "activerecord.models.#{model_name.downcase}", default: ".#{model_name}."
  end

  def set_current_user
    User.current = current_user if current_user.present?
  end

  def list
    render "list"
  end

  def set_object
    if ["show", "update", "destroy", "edit"].include? params[:action]
      @object = model.find(params[:id]) rescue nil
    elsif ["new", "create"].include? params[:action]
      if @permit_params.present?
        @object = model.new(send(@permit_params)) rescue nil
      elsif self.respond_to?(model.to_s.parameterize + "_params", true)
        @object = model.new(send("#{model.to_s.parameterize}_params")) rescue nil
      else
        @object = model.new(permit_params) rescue nil
      end
    end
    instance_variable_set("@#{params[:controller]}", @object) rescue nil
  end
end
