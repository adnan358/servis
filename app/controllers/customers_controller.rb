class CustomersController < ApplicationController
  before_action :set_customer, only: [:show, :edit, :update, :destroy]
  before_action :customer_list, only: [:index, :list, :create]

  def index
    @customers = Customer.all
  end

  def show
    render_modal "Müşteri Detayı"
  end

  def edit
    @customer_services = @customers.services
    render_modal "Müşteri Detayı"
  end

  def new
    @customer = Customer.new
    render_modal "Müşteri Kayıt"
  end

  def create
    respond_to do |format|
      if @object.save
        format.js { render "create" }
      else
        format.js { render "errors" }
      end
    end
  end

  def update
    respond_to do |format|
      if @object.update(permit_params)
        flash[:success] = "Müşteri güncellendi"
        format.js { render "#{params[:action]}" }
        format.html { render "#{params[:action]}"}
      else
        format.js { render "errors" }
      end
    end
  end

  def list
    render "list"
  end


  private

  def customer_list
    @customers = current_user.customers.all
  end

    # Use callbacks to share common setup or constraints between actions.
    def set_customer
      @customer = Customer.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def permit_params
      params.fetch(:customer, {}).permit(:name, :phone, :address, :district_id, services_attributes: [:address, :district_id, :device_type_id, :brand_id, :defect, :description, :appointment_date, :subcontractor_id])
    end
end
