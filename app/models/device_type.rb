# == Schema Information
#
# Table name: device_types
#
#  id          :bigint           not null, primary key
#  device_name :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
class DeviceType < ApplicationRecord
	has_many :service
end
