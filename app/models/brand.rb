# == Schema Information
#
# Table name: brands
#
#  id         :bigint           not null, primary key
#  brand_name :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Brand < ApplicationRecord
	has_many :service
end
