# == Schema Information
#
# Table name: services
#
#  id               :bigint           not null, primary key
#  address          :string
#  appointment_date :datetime
#  defect           :string
#  description      :string
#  mobile_phone     :string
#  name             :string
#  phone            :string
#  status           :string           default("0")
#  surname          :string
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  brand_id         :bigint           not null
#  customer_id      :integer          not null
#  device_type_id   :bigint           not null
#  subcontractor_id :integer
#  user_id          :bigint           not null
#
# Indexes
#
#  index_services_on_brand_id        (brand_id)
#  index_services_on_device_type_id  (device_type_id)
#  index_services_on_user_id         (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (brand_id => brands.id)
#  fk_rails_...  (device_type_id => device_types.id)
#  fk_rails_...  (user_id => users.id)
#
class Service < ApplicationRecord
  audited
  belongs_to :device_type
  belongs_to :brand
  belongs_to :user
  belongs_to :customer

  validates :defect, presence: true
  validates :appointment_date, presence: true

  enum state: ['Bekliyor', 'Teslim edildi, tamamlandı', 'Müşteri Tarafından İptal', 'Bizim Tarafımızdan İptal', 'Tekrar', 'Atölyede, tamir ediliyor']


  after_initialize do |service|
    service.user = User.current
  end

  def display
    self.brand_name + " " + self.device_name
  end

  def brand_name
  	self.brand[:brand_name]
  end

  def device_name
  	self.device_type[:device_name]
  end

  def district_name
  	self.district[:discrict_name]
  end

  def get_status
  	Service.states.key(self.status.to_i)
  end

  def full_address
    self.address + " " + self.district_name
  end

end
