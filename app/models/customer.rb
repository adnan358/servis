# == Schema Information
#
# Table name: customers
#
#  id          :bigint           not null, primary key
#  address     :string
#  name        :string
#  phone       :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  district_id :integer          not null
#  user_id     :integer          not null
#
class Customer < ApplicationRecord
  has_many :services
  belongs_to :district
  accepts_nested_attributes_for :services

  validates :name, presence: true
  validates :phone, presence: true
  validates :address, presence: true


  def self.default_scope
    where(user_id: User.current.id) if User.current.present?
  end

  after_initialize do |customer|
    customer.user_id = User.current.id if User.current.present?
  end

  def self.create_customer_or_service(params)
    customer = self.find_or_initialize_by(phone: params[:phone])
    if customer.new_record?
      self.new(params)
    else
      customer.services.new params[:services_attributes]["0"]
    end
  end

  def full_address
    (self.address + " " + self.district_name).gsub!("\\", "\\\\\\\\")
  end

  def district_name
    self.district[:discrict_name]
  end

end
