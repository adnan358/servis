# == Schema Information
#
# Table name: subcontractors
#
#  id          :bigint           not null, primary key
#  credit      :float
#  description :text
#  name        :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  service_id  :bigint           not null
#  user_id     :bigint           not null
#
# Indexes
#
#  index_subcontractors_on_service_id  (service_id)
#  index_subcontractors_on_user_id     (user_id)
#
class Subcontractor < ApplicationRecord
  has_many :services
  belongs_to :user

  before_destroy :can_destroy?

  after_initialize do |subcontractor|
    subcontractor.user = User.current
  end

  private

  def can_destroy?
    if self.services.count > 0
      self.errors.add(:base, "Bu taşerona verilmiş servisler olduğu için silemezsiniz.")
      throw :abort
    end
  end

end
