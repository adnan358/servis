# == Schema Information
#
# Table name: services
#
#  id               :bigint           not null, primary key
#  address          :string
#  appointment_date :datetime
#  defect           :string
#  description      :string
#  mobile_phone     :string
#  name             :string
#  phone            :string
#  status           :string           default("0")
#  surname          :string
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  brand_id         :bigint           not null
#  customer_id      :integer          not null
#  device_type_id   :bigint           not null
#  subcontractor_id :integer
#  user_id          :bigint           not null
#
# Indexes
#
#  index_services_on_brand_id        (brand_id)
#  index_services_on_device_type_id  (device_type_id)
#  index_services_on_user_id         (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (brand_id => brands.id)
#  fk_rails_...  (device_type_id => device_types.id)
#  fk_rails_...  (user_id => users.id)
#
require 'test_helper'

class ServiceTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
