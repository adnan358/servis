# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


device_type = ["Bulaşık Makinesi","Buzdolabı","Çamaşır Makinesi","Fırın","Kahve makinesi","Kazan Tamiri","Klima","Kombi","Kurutma Makinesi","Ocak","Süpürge","Televizyon","Termosifon","Ticari Buzdolabı"]
device_type.each do |type|
	DeviceType.create(device_name: type)
end

districts = ["Aliağa","Alsancak","Altındağ","Balçova","Bayraklı","Bornova","Bostanlı","Bozyaka","Buca","Çeşme","Çiğli","Gaziemir","Göztepe","Güzelbahçe","Güzelyalı","Harmandalı","Hatay","Karabağlar","Karşıyaka","Konak","Manisa","Mavişehir","Menderes","Menemen","Narlıdere","Özdere","Pınarbaşı","Torbalı","Urla","Yeşilova","Yeşilyurt"]
districts.each do |d|
	District.create(discrict_name: d)
end

brands = ["AEG","Airfel","Altus","Arçelik","Ariston","Baymak","Beko","Bosch","Buderus","Daikin","Demirdöküm","ECA","Elektrolüx","Falce","Ferroli","Hitachi","İndesit","LG","Maktek","Profilo","Regal","Roventa","Rubenis","Samsung","Siemens","Şenocak","Termikel","Termodinamik","Vaillant","Vestel","Viessman","Whirpool","Hoover"]
brands.each do |b|
	Brand.create(brand_name: b)
end