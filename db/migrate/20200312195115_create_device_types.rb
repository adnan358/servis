class CreateDeviceTypes < ActiveRecord::Migration[6.0]
  def change
    create_table :device_types do |t|
      t.string :device_name

      t.timestamps
    end
  end
end
