class AddCustomerIdToService < ActiveRecord::Migration[6.0]
  def change
    add_column :services, :customer_id, :integer, null: false
  end
end
