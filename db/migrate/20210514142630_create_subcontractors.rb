class CreateSubcontractors < ActiveRecord::Migration[6.0]
  def change
    create_table :subcontractors do |t|
      t.string :name
      t.float :transaction
      t.references :user, null: false, foreign_key: false
      t.text :description

      t.timestamps
    end
  end
end
