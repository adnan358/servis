class AddDistrictColumn < ActiveRecord::Migration[6.0]
  def change
    add_column :customers, :district_id, :integer, null: false, foreign_key: true
  end
end
