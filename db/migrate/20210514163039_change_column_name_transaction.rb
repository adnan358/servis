class ChangeColumnNameTransaction < ActiveRecord::Migration[6.0]
  def change
    rename_column :subcontractors, :transaction, :credit
  end
end
