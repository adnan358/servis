class ChangeSubcontractorColumnName < ActiveRecord::Migration[6.0]
  def change
    rename_column :services, :subcontractor, :subcontractor_id
  end
end
