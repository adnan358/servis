class ChangeSubcontructColumnType < ActiveRecord::Migration[6.0]
  def change
    change_column :services, :subcontractor, 'integer USING CAST(subcontractor AS integer)'
  end
end
