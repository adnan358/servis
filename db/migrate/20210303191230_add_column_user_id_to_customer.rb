class AddColumnUserIdToCustomer < ActiveRecord::Migration[6.0]
  def change
    add_column :customers, :user_id, :integer

    Customer.unscoped.all.each do |customer|
      customer.user_id = 1
      customer.save!
    end

    change_column :customers, :user_id, :integer, null: false
  end
end
