class AddStatusToService < ActiveRecord::Migration[6.0]
  def change
    add_column :services, :status, :string, :default => 0
  end
end
