class CreateServices < ActiveRecord::Migration[6.0]
  def change
    create_table :services do |t|
      t.string :name
      t.string :surname
      t.string :address
      t.string :mobile_phone
      t.string :phone
      t.string :defect
      t.references :device_type, null: false, foreign_key: true
      t.references :brand, null: false, foreign_key: true
      t.boolean :subcontractor
      t.string :description
      t.datetime :appointment_date

      t.timestamps
    end
  end
end
